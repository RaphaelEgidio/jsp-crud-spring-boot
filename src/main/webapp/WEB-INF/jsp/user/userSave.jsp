<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Inserir / Editar - Usu�rio</title>
    
    <link href="<c:url value="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />" rel="stylesheet" type="text/css" />
	<script src="<c:url value="/webjars/bootstrap/4.1.3/js/bootstrap.min.js" />" type="text/javascript"></script>
	
</head>
<body>
	<h1>Inserir / Editar - Usu�rio</h1>
	<a href='<c:url value="/user" />'>Voltar</a>

	<form:form id="formId" action="/exemplo/user/save" method="post"
		modelAttribute="user">

		<div>
			<form:label path="login"
				cssStyle="width:80px; text-align:right;display:inline-block">Login:</form:label>
			<form:input path="login" />
		</div>
		<div>
			<form:label path="password"
				cssStyle="width:80px; text-align:right;display:inline-block">Password:</form:label>
			<form:input path="password" />
		</div>

		<form:hidden path="id" />
		<form:hidden path="enable" />
		<form:hidden path="createDate" />
		<form:hidden path="updateDate" />

		<form:button class="btn btn-success">Enviar</form:button>
	</form:form>

</body>
</html>