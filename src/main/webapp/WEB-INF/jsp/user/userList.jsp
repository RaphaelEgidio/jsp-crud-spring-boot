<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<h1>Listar - Usu�rio</h1>
	<a href='<c:url value="/user/save" />'>Incluir</a> /
	<a href='<c:url value="/" />'>Voltar</a>
	<div>
		<table border="1">
			<thead>
				<tr>
					<th>C�digo</th>
					<th>Login</th>
					<th>Editar</th>
					<th>Desativar</th>
				</tr>
			</thead>
			<c:forEach var="user" items="${users}">
				<tr>
					<td>${user.id}</td>
					<td>${user.login}</td>
					<td><a href='<c:url value="/user/edit/${user.id}" />'>Editar</a></td>
					<td><a href='<c:url value="/user/delete/${user.id}" />'>Desativar</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>

</body>
</html>