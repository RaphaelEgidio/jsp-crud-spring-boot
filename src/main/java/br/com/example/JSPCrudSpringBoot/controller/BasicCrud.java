package br.com.example.JSPCrudSpringBoot.controller;

import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

public abstract class BasicCrud<T> extends BasicController {

	public abstract ModelAndView save(final T object, final Model model);

	public abstract ModelAndView edit(final Long id);

	public abstract ModelAndView delete(final Long id);

	public abstract ModelAndView findAll(final Boolean enable);

	protected String redirect(final String url) {
		return "redirect:" + url;
	}

}
