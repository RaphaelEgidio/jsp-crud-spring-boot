package br.com.example.JSPCrudSpringBoot.controller;

import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

public abstract class BasicController {

	public abstract ModelAndView initialize(Model model);

}
