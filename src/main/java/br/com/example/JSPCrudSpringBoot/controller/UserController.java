package br.com.example.JSPCrudSpringBoot.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.example.JSPCrudSpringBoot.entity.User;
import br.com.example.JSPCrudSpringBoot.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController extends BasicCrud<User> {

	public static final String USER_LIST = "user/userList";
	public static final String USER_SAVE = "user/userSave";

	@Autowired
	private UserService userService;

	@RequestMapping(method = GET)
	public ModelAndView initialize(final Model model) {
		return new ModelAndView(UserController.USER_LIST, "users", userService.findAll(true));
	}

	@RequestMapping(value = { "/save" }, method = GET)
	public ModelAndView include() {
		return new ModelAndView(UserController.USER_SAVE).addObject("user", new User());
	}

	@RequestMapping(value = { "/save" }, method = POST)
	public ModelAndView save(@ModelAttribute("user") final User entity, final Model model) {
		try {
			userService.save(entity);
			return new ModelAndView(this.redirect("/user"));
		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView(UserController.USER_SAVE).addObject("user", entity);
		}
	}

	@RequestMapping(value = { "/edit/{id}" }, method = GET)
	public ModelAndView edit(@PathVariable("id") final Long id) {
		ModelAndView modelAndView = new ModelAndView(UserController.USER_SAVE);

		modelAndView.addObject("user", userService.findOne(id));

		return modelAndView;
	}

	@RequestMapping(value = { "/findAll/{enable}" }, method = GET)
	public ModelAndView findAll(@PathVariable("enable") final Boolean enable) {

		return new ModelAndView("listUser");
	}

	@RequestMapping(value = { "/delete/{id}" }, method = GET)
	public ModelAndView delete(@PathVariable("id") final Long id) {
		try {
			userService.disableOrEnable(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("listUser");
	}
}