package br.com.example.JSPCrudSpringBoot.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.example.JSPCrudSpringBoot.entity.User;
import br.com.example.JSPCrudSpringBoot.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User save(final User user) throws Exception {
		userExists(user);
		return userRepository.save(user);
	}

	public User findOne(final Long id) {
		final Optional<User> optional = userRepository.findById(id);
		return optional.isPresent() ? optional.get() : null;
	}

	public List<User> findAll(final Boolean enable) {
		return userRepository.findByEnable(enable);
	}

	public void disableOrEnable(final Long id) throws Exception {
		final User user = findOne(id);
		if (user != null) {
			user.setEnable(!user.getEnable());
			save(user);
		}
	}

	public void userExists(final User user) throws Exception {
		if (Objects.isNull(user) || !Objects.isNull(user.getId())) {
			return;
		}
		final User exist = userRepository.findByLogin(user.getLogin());
		if (!Objects.isNull(exist)) {
			throw new Exception("Usuário já esta sendo utilizado!");
		}
	}

}
